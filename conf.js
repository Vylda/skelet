var includePaths = require("rollup-plugin-includepaths");
var filesize = require('./filesize');

const CONF = {
	watcher: [{
		path: "src",
		type: "js"
	}, {
		path: "less",
		type: "less"
	}],
	babel: {
		input: "./dist/main.js",
		dest: "./dist/main-legacy.js",
		compress: "./dist/main-legacy.min.js"
	},
	rollup: {
		input: "./src/main.js",
		output: {
			file: "./dist/main.js",
			format: "iife",
			name: "main"
		},
		compress: "./dist/main.min.js"
	},
	less: {
		file: "./less/main.less",
		output: "./dist/main.css",
		paths: ["less"],
		plugins: {
			"autoprefix": "Android 2.3,Android >= 4,Chrome >= 35,Firefox >= 30,Explorer >= 10,iOS >= 8,Opera >= 21,Safari >= 7",
			"clean-css": "--s1 --advanced --compatibility=ie8"
		}
	}
};

exports.CONF = CONF;

exports.getRollup = () => {
	return {
		input: {
			input: CONF.rollup.input,
			plugins: [includePaths({ paths: ["src"] }), filesize()]
		},
		output: CONF.rollup.output
	};
};
