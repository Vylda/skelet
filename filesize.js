'use strict';

var fileSize = require("filesize");
var boxen = require("boxen");
var colors = require("colors");
var deepAssign = require("deep-assign");
var gzip = require("gzip-size");
var brotli = require("brotli-size");
var terser = require("terser");

var toConsumableArray = function(arr) {
	if (Array.isArray(arr)) {
		for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

		return arr2;
	} else {
		return Array.from(arr);
	}
};

function render (opt, outputOptions, sizes) {
	var primaryColor = opt.theme === "dark" ? "green" : "black";
	var secondaryColor = opt.theme === "dark" ? "yellow" : "blue";

	var title = colors[primaryColor].bold;
	var value = colors[secondaryColor];

	let strDate = "";

	if (opt.showTime) {
		let date = new Date();
		strDate = [
			date.getDate().toString().padStart(2, "0"),
			(date.getMonth() + 1).toString().padStart(2, "0"),
			date.getFullYear()
		].join(".") + " " +
			[
				date.getHours().toString().padStart(2, "0"),
				date.getMinutes().toString().padStart(2, "0"),
				date.getSeconds().toString().padStart(2, "0")
			].join(":") + "." + date.getMilliseconds().toString().padStart(3, "0");
	}

	var values = [].concat(toConsumableArray(strDate ? ["" + title("Time: ") + value(strDate)] : []), toConsumableArray(outputOptions.file ? ["" + title("Destination: ") + value(outputOptions.file)] : []), [title("Bundle Size: ") + " " + value(sizes.bundleSize)], toConsumableArray(sizes.minSize ? [title("Minified Size: ") + " " + value(sizes.minSize)] : []), toConsumableArray(sizes.gzipSize ? [title("Gzipped Size: ") + " " + value(sizes.gzipSize)] : []), toConsumableArray(sizes.brotliSize ? ["" + title("Brotli size: ") + value(sizes.brotliSize)] : []));

	return boxen(values.join("\n"), { padding: 1 });
}

function filesize () {
	var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	var env = arguments[1];

	var defaultOptions = {
		format: {},
		theme: "dark",
		render: render,
		showGzippedSize: true,
		showBrotliSize: false,
		showMinifiedSize: true,
		showTime: true
	};

	var opts = deepAssign({}, defaultOptions, options);
	if (options.render) {
		opts.render = options.render;
	}

	var getData = function getData (outputOptions, code) {
		var sizes = {};
		sizes.bundleSize = fileSize(Buffer.byteLength(code), opts.format);

		sizes.brotliSize = opts.showBrotliSize ? fileSize(brotli.sync(code), opts.format) : "";

		if (opts.showMinifiedSize || opts.showGzippedSize) {
			var minifiedCode = terser.minify(code).code;
			sizes.minSize = opts.showMinifiedSize ? fileSize(minifiedCode.length, opts.format) : "";
			sizes.gzipSize = opts.showGzippedSize ? fileSize(gzip.sync(minifiedCode), opts.format) : "";
		}

		return opts.render(opts, outputOptions, sizes);
	};

	if (env === "test") {
		return getData;
	}

	return {
		name: "filesize",
		generateBundle: function generateBundle (outputOptions, bundle, isWrite) {
			Object.keys(bundle).map(function(fileName) {
				return bundle[fileName];
			}).filter(function(currentBundle) {
				return !currentBundle.isAsset;
			}).forEach(function(currentBundle) {
				console.log(getData(outputOptions, currentBundle.code));
			});
		}
	};
}

module.exports = filesize;
